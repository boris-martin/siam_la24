import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 16})

# Define the z range from 0 to 1
z = np.linspace(0, 1, 300)

# Define different alpha values
alpha_values = [0, 1, 2, 5]

# Create the plot
plt.figure(figsize=(12, 6))

# Loop through each alpha and plot the function
for alpha in alpha_values:
    y = 1 + alpha * np.sin(3 * np.pi * z)**2
    plt.plot(y, z, label=f'$\\alpha = {alpha}$')

# Adding labels, title, and legend
plt.xlabel('$\\frac{c}{c_0}$')
plt.ylabel('$\\frac{z}{H}$')
plt.title('Velocity profile along the depth')
plt.xlim(0, 1+max(alpha_values))
plt.legend(loc='center left')
plt.grid(True)

# Show the plot
plt.savefig('gen_depth.png', dpi=300)
plt.show()
