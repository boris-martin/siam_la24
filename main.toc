\beamer@sectionintoc {1}{GMRES and BGMRES}{35}{0}{1}
\beamer@sectionintoc {2}{Previous Work}{39}{0}{2}
\beamer@sectionintoc {3}{Cost on real hardware}{42}{0}{3}
\beamer@sectionintoc {4}{Cost of orthogonalization and blocks}{42}{0}{4}
\beamer@sectionintoc {5}{Convergence}{50}{0}{5}
