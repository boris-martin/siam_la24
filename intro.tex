
\begin{frame}[t]{Full waveform inversion (FWI)}
  FWI is an \textbf{imaging method} that reconstructs physical properties of a
  sample by \textbf{minimizing} the mismatch between measured wave scattering
  data on the boundary of the sample and data obtained by \textbf{full-wave
    simulations}
  \begin{columns}
    \begin{column}{0.7\textwidth}%
      \begin{overprint}%
        \includegraphics<2-3>[scale=1.15]{anim/model0.eps}%
        \includegraphics<4>[scale=1.15]{anim/data0.eps}%
        \includegraphics<5>[scale=1.15]{anim/data_ref.eps}%
        \includegraphics<6-7>[scale=1.15]{anim/model1.eps}%
        \includegraphics<8>[scale=1.15]{anim/data1.eps}%
        \includegraphics<9-10>[scale=1.15]{anim/model2.eps}%
        \includegraphics<11>[scale=1.15]{anim/data2.eps}%
        \includegraphics<12-13>[scale=1.15]{anim/model3.eps}%
        \includegraphics<14>[scale=1.15]{anim/data3.eps}%
        \includegraphics<15-16>[scale=1.15]{anim/model4.eps}%
        \includegraphics<17>[scale=1.15]{anim/data4.eps}%
        \includegraphics<18>[scale=1.15]{anim/stop.eps}%
      \end{overprint}%
    \end{column}
    \begin{column}{0.3\textwidth}%
      \begin{overprint}%
        \only<3>{
          \small{Simulate~the~propagation}\par
          \animategraphics[width=0.9\textwidth,height=0.611\textwidth,autoplay,controls=none]{12}{anim/IT0/snapshot}{0}{74}
        }%
        \only<7>{
          \small{Simulate~the~propagation}\par
          \animategraphics[width=0.9\textwidth,height=0.611\textwidth,autoplay,controls=none]{12}{anim/IT1/snapshot}{0}{74}
        }%
        \only<10>{
          \small{Simulate~the~propagation}\par
          \animategraphics[width=0.9\textwidth,height=0.611\textwidth,autoplay,controls=none]{12}{anim/IT2/snapshot}{0}{74}
        }%
        \only<13>{
          \small{Simulate~the~propagation}\par
          \animategraphics[width=0.9\textwidth,height=0.611\textwidth,autoplay,controls=none]{12}{anim/IT3/snapshot}{0}{74}
        }%
        \only<16>{
          \small{Simulate~the~propagation}\par
          \animategraphics[width=0.9\textwidth,height=0.5431\textwidth,autoplay,controls=none]{12}{anim/IT4/snapshot}{0}{74}
        }
        \only<18>{\hspace{5ex}\textcolor{mypurple}{\scriptsize[\textit{Adriaens
              et al. 2023}]}}
      \end{overprint}
    \end{column}
  \end{columns}
  \bigskip
  \only<18>{
    This is FWI in the time domain: we will use it in the \textbf{frequency
      domain}, solving the Helmholtz equation instead of the wave equation
  }
\end{frame}

\begin{frame}{FWI in the frequency domain}
  \textbf{Problem statement}: For a model $m(x)$, a wavefield $u(x)$, data $d$,
  excitation $f$ and a measurement operator $R$, find $m$ that minimizes
  $J(m) = \| R u(m) - d \|^{2}_{2}$ under constraint $A(m) u = f$
  \\[2ex]\pause
  Setup for this talk:
  \begin{itemize}
  \item the model $m(x)$ is the local wave speed $c(x)$ in a 2D rectangular
    domain $\Omega$
  \item $A(m)$ is the Helmholtz operator, i.e. $u$ satisfies the Helmholtz
    equation $- \Delta u - \frac{\omega^2}{c(x)^2} u = f$, with $\omega$ the
    angular frequency
  \item the excitation $f$ consists in (potentially many) point sources located
    near the top of $\Omega$
  \end{itemize}
  \medskip
  \pause
  The minimization is carried out using a local, gradient-based optimization
  method (typically l-BFGS): computing $J(m)$ and $\nabla{J(m)}$
  requires solving 2 Helmholtz problems, using an adjoint approach\\[2ex]
  \pause \textbf{Main cost}: solve $A(m)u = f$ for different $f$ and $m$
\end{frame}

% \begin{frame}{Full Waveform Inversion (in the frequency domain)}
%   \begin{figure}
%       \centering
%       \includegraphics[width=0.67\textwidth,height=0.40\textwidth,keepaspectratio]{figs/brain.png}\\
%       \caption{Imaginary part of permittivity in the brain. \textit{Tournier
%       et al., 2019, Microwave tomographic imaging of cerebrovascular accidents
%       by using high-performance computing}}
%   \end{figure}
% \end{frame}

% \begin{frame}{Full Waveform Inversion (in the frequency domain)}
%   \begin{figure}
%       \centering
%       \includegraphics[width=0.6\textwidth,height=0.36\textwidth,keepaspectratio]{figs/marmousi_final.png}
%       \includegraphics[width=0.6\textwidth,height=0.36\textwidth,keepaspectratio]{figs/marmousi_initial.png}\\
%       \caption{Slowness squared of the Marmousi model, a common geophysics
%       benchmark for FWI (Target above, initial guess below)}
%   \end{figure}
% \end{frame}

\begin{frame}{Domain Decomposition Methods}
  High-resolution FWI requires $\omega \gg$, leading to large-scale complex
  and indefinite linear systems for which \textbf{direct solvers don't scale}
  and \textbf{standard iterative methods fail}
  \textcolor{mypurple}{\scriptsize[\textit{Ernst, Gander 2011}]}\\[3ex]\pause
  With Domain Decomposition Methods (DDM) we can either:
  \begin{itemize}
  \item Build a preconditioner made of local solves (e.g. ORAS)
  \item Solve an \textbf{interface problem} to glue local solutions together (e.g. OSM)
  \end{itemize}
  \bigskip
  \underline{Which one to use for FWI?}
\end{frame}

\begin{frame}{Model problem: Helmholtz equation}

  We focus on the Helmholtz equation, but generalizations to other PDEs (Maxwell, elastodynamics) are natural.
  Using Fourier transform with $e^{-\imath \omega t}$ time dependence, we get:

\begin{block}{Helmholtz equation}
  $$
  \left\{
    \begin{array}{ll}{
      -\Delta u - k^2 u = f
      \text { in } \Omega_{i}, } & (\text{Helmholtz equation}) \\
      (\partial_{\boldsymbol{n}} u
      - \imath k u)=0
      \text {, on } \Gamma^{\infty} &  (\text{radiation condition}) \\
      
    \end{array}
  \right.
  $$

  with $k = \frac{\omega}{c(x)}$ the wave number, complex-valued in presence of damping.
\end{block}

\end{frame}


\begin{frame}{Optimized Restrictive Additive Schwarz (ORAS) for Helmholtz}
  Partition $\Omega$ into \textbf{overlapping} subdomains $\Omega_i$,
  $i=1, \dots, N_{\text{dom}}$, define a restriction map $R_i$ from $\Omega$ to $\Omega_i$ and a partition of unity $D_i$ such that
  $\sum_i^{N_{\text{dom}}} R_i^T D_i R_i = I$. Let $A$ be the Helmholtz operator for a given discretization.
  \begin{block}{RAS and ORAS preconditioners}
    Let $A_{\text{loc},i} = R_i A R_i^T$ the local matrix in domain $i$. The RAS preconditioner is given by
    $$
    M^{-1}_{\text{RAS}} = \sum_{i=1}^{N_{\text{dom}}} R_i^T D_i A^{-1}_{\text{loc},i} R_i.
    $$
    If we replace $A_{\text{loc},i}$ by a local Helmholtz operator $A_{\text{imp},i}$ with impedance (Robin) BCs on the artificial boundaries, we get the improved ORAS preconditioner:
    $$ 
    M^{-1}_{\text{ORAS}} = \sum_{i=1}^{N_{\text{dom}}} R_i^T D_i A^{-1}_{\text{imp},i} R_i.
    $$
    
  \end{block}
  \bigskip
  \pause
  Then, we solve with GMRES (or related) the linear system $M^{-1}_{\text{ORAS}} A u = M^{-1}_{\text{ORAS}} f$.
\end{frame}

\begin{frame}{ORAS in PETSc}

  Easy to use in PETSc with the \texttt{PCASM} preconditioner:
  \begin{itemize}
  \item Provide the local matrices $A_{\text{loc},i}$.
  \item Provide Index Sets that describe the overlaps.
  \item Set the Sub-KSP to be a LU solve.
  \end{itemize}

\end{frame}


\begin{frame}{Non-Overlapping Optimized Schwarz Method (OSM) for Helmholtz}
  Partition $\Omega$ into \textbf{non-overlapping} subdomains $\Omega_i$,
  $i=1, \dots, N_{\text{dom}}$, with interface $\Sigma_{i,j}$ between $\Omega_i$
  and $\Omega_j$. In each subdomain $\Omega_i$, solve the boundary value
  problem
  \begin{block}{Non-overlapping optimized Schwarz formulation}
    $$
    \left\{
      \begin{array}{ll}{
        -\Delta u_i - k^2 u_i = f
        \text { in } \Omega_{i}, } & (\text{Helmholtz equation}) \\
        (\partial_{\boldsymbol{n}_{i}} u_{i}
        - \imath k u_{i})=0
        \text {, on } \Gamma_i^{\infty} &  (\text{radiation condition}) \\
        \left(\partial_{\boldsymbol{n}_{i}} u_{i}
        - \textcolor{blue}{\mathcal{S}} u_{i}\right)=g_{ij}
        \text {, on } \Sigma_{ij} & (\text{interface condition})
      \end{array}
    \right.
    $$
    with $k = \frac{\omega}{c(x)}$ the wave number and
    $\textcolor{blue}{\mathcal{S}}$ a well-chosen interface operator, e.g. $\mathcal{S} = \imath k$.
    In practice, we use an 2nd order optimized condition:
    $$ \mathcal{S} = \imath k (\cos{\phi} + \frac{1}{2k^2}e^{-\imath \phi}\Delta_\Sigma). $$
    Experiments show $\phi = \frac{\pi}{2}$ is a good choice.
    
  \end{block}
  \bigskip
  \pause
  Introduce the interface coupling on $\Sigma_{ij}$
  \begin{align*}
    g_{ij} = - g_{ji} + 2 \mathcal{S} u_j := \mathcal{T}_{ji} g_{ji} + b_{ji}
  \end{align*}
\end{frame}

\begin{frame}{Substructured DDM}
  Rewrite the coupling as a linear system for $g=(g_{ij}, g_{ji})^T$:
  \begin{align*}
    \underbrace{A}_{\text{iteration matrix}} \;
    \underbrace{g}_{\text{interface unknowns}}  =
    \underbrace{b}_{\text{physical sources}}, \quad A = I -
    \left(\begin{array}{cc}
            0 & \mathcal{T}_{j i} \\
            \mathcal{T}_{i j} & 0
          \end{array}\right)
  \end{align*}\bigskip
  We solve this linear system with a matrix-free \textbf{Krylov solver} such as
  GMRES or GCR\\[2ex]\pause
  Properties of the interface problem:
  \begin{itemize}
  \item Significantly smaller number of unknowns than the volume problem
  \item Eigenvalues are in the unit ball centered on 1 for ``good'' $\mathcal{S}$
  \item One matrix-vector product involves solving each subproblem once
  \end{itemize}
  \bigskip
  \textbf{Solving the subproblems using a sparse direct solver is the most
    computationally expensive part}
\end{frame}

\begin{frame}{OSM in PETSc}
  OSM is trickier to use in PETSc. We define a \texttt{MatShell} object that:
  \begin{itemize}
  \item Assemble the incoming wave from the input vector.
  \item Solves the subproblems with a direct solver.
  \item Computes the outgoing wave and maps them to the output vector.
  \end{itemize}
  \bigskip
  \textbf{Optimized implementation is quite involved.}

\end{frame}



\begin{frame}{Comparison of the methods}
  \begin{block}{Substructured DDM (OSM)}
    \begin{itemize}
    \item Non-overlapping subdomains
    \item Solve (with GMRES) a linear system for the interface unknowns (\textbf{which is smaller!})
    \item Local solves are part of the operator application
    \end{itemize}
  \end{block}
  \begin{block}{ORAS}
    \begin{itemize}
    \item Overlapping subdomains
    \item Invert the global FEM matrix with GMRES 
    \item Use local solves to build a preconditioner
    \end{itemize}
  \end{block}

  N.B.: The OSM method is also known as FETI-2LM.

  \bigskip
  \pause
  \textbf{Which one to use for FWI? How to handle multiple sources?}
\end{frame}
