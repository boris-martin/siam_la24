\documentclass[10pt,usenames,dvipsnames,aspectratio=1610,mathserif]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usetikzlibrary{positioning, calc, shapes, arrows, matrix, fit, backgrounds}
\usepackage[absolute,overlay]{textpos}
\usepackage[misc]{ifsym} % \Letter
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{standalone}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepackage{animate}
\usepackage{siunitx}
\usepackage[normalem]{ulem}
\usepackage{soul}
\usepackage{hyperref}
\usepackage{multirow}
\usepackage{tikzsymbols}
\usepackage{xspace}
\usepackage{xcolor, colortbl}
\usepackage{booktabs}% http://ctan.org/pkg/booktabs

\definecolor{darkgreen}{rgb}{0.09, 0.45, 0.27}
\definecolor{myred}{rgb}{0.92, 0.0, 0.0}
\definecolor{mypurple}{rgb}{0.3, 0.3, 0.3}
\setbeamertemplate{navigation symbols}{}%remove navigation symbols
\usetheme{Singapore}
\usecolortheme{whale}
\setbeamertemplate{blocks}[rounded]
\setbeamercolor{block body}{bg=gray!20,fg=black}
\setbeamercolor{block title}{bg=structure!80,fg=white}
\captionsetup{labelformat=empty}

\makeatletter
\newcommand{\gettikzxy}[3]{%
  \tikz@scan@one@point\pgfutil@firstofone#1\relax
  \edef#2{\the\pgf@x}%
  \edef#3{\the\pgf@y}%
}
\makeatother
\title{\textbf{\large Efficient Handling of Multiple Sources in\\[0.4ex]
    Domain Decomposition Methods for \\Full Waveform Inversion
    }}

\author{\underline{Boris Martin}\textsuperscript{1}, Christophe Geuzaine\textsuperscript{1} and Pierre Jolivet\textsuperscript{2}
  \texorpdfstring{\vspace{0.2cm}} \small{\\
  \textsuperscript{1}Université de Liège \\
  \textsuperscript{2}Sorbonne Université, Paris}}

\date{}
\date{\small PETSc User Meeting, May 23-24, 2024}

\setbeamercolor{section in head/foot}{fg=black}
\setbeamertemplate{headline}{}
\setbeamercolor{footlinerule}{use=structure,bg=structure.fg!100!bg}
\setbeamertemplate{footline}{%
  \hbox{%
  \begin{beamercolorbox}[wd=\textwidth,ht=2.5ex,dp=1.5ex,left]{background}%
    \usebeamerfont{author in head/foot} \hspace*{2ex}PETSc User Meeting, May 23-24, 2024\hspace*{13.5cm}\insertframenumber{}
  \end{beamercolorbox}%
  }%
  \vskip0pt%
}%

\setbeamertemplate{itemize subitem}{$\rightarrow$}

\begin{document}

{
  \setbeamertemplate{footline}{}
  \setbeamertemplate{headline}{}
  \begin{frame}
    \titlepage
    \vspace{5ex}
    \begin{center}
      \includegraphics[height=5ex]{figs/Liege_logo.png}
      \hspace*{1cm}\includegraphics[height=5.5ex]{figs/fnrs.png}
    \end{center}
  \end{frame}
}

\input{intro.tex}
\section{GMRES and BGMRES}
\input{gmres.tex}
\section{Previous Work}
\input{previously.tex}
\section{Cost on real hardware}
\input{orthogo_cost.tex}
\input{convergence.tex}

% CONCLUSIONS TALK
\begin{frame}{Conclusion}
We compared two domain decomposition methods for solving the Helmholtz equation, ORAS and OSM.\\[2ex]
For both approaches, we assessed the relevance of using BGMRES instead of GMRES.

\begin{itemize}
  \item[\checkmark] Substructuring efficiently reduces the cost of orthogonalization.
  \item[\checkmark] Block methods sometimes have a significant boost on convergence, especially in highly heterogeneous media.
  \item[\times] The best method is heavily problem-dependent.
  \item[\times] Optimized implementation needed for a fair benchmarking.
\end{itemize}
\end{frame}

\begin{frame}{Future work \& open questions}
  
  \begin{itemize}
    \item Combination with subspace recycling (BGCRO-DR, Block Orthodir with rank revealing QR at each iteration).
    \item Interaction with coarse grids, such as GenEO.
    \item Unstructured (automatic) partitioning.
    \item Comparison with the best transmission conditions (PML, Padé).
    \item Recycling from one velocity model to another.
    \item Substructured overlapping methods?
  \end{itemize}
  \pause
  \bigskip
  \begin{center}{\large Thanks for your attention}\\[1ex]
    {\small \Letter \hspace{.5em} boris.martin@uliege.be}
  \end{center}

\end{frame}

\begin{frame}{Effect of Substructuring}
  How much smaller is the reduced problem?\\
  \textbf{It depends on the surface/volume ratio of the subdomains.}
  Rule of thumb: for a cube of size $n$, the surface is $6n^2$ and the volume is $n^3$.\\
  Surface to volume ratio is then $6/n$.\\
  For domains around 50k to 100k DOFs, \textbf{the reduction factor is typically between 5 and 10}.\\
  \vspace{1cm}
  (In 2D, the reduction factor can go beyond 100!)
\end{frame}


\begin{frame}{Neglected aspects}
  \begin{itemize}
      \item For ORAS: Sparse matrix-vector product. (Cheap and efficiently implemented in PETSc)
      \item OSM: assembly of the interface problem. Very expensive in our implementation but easily optimized: replace the assembly by an explicit sparse matrix-vector product.
      \item We have not considered variable overlap sizes.
      \item Optimized transmission conditions for ORAS were not used, for simplicity.
  \end{itemize}

\end{frame}


\end{document}
