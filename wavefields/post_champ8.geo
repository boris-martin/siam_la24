// GMSH script to open a .pos file and export as PNG

// Replace 'your_file.pos' with the actual file path
Merge "marmousi_50_f_8_rhs_0_rank_0.msh";

//Plugin (MathEval).Expression0 = "1/sqrt(v0+1e-8)";
//Plugin (MathEval).Run;
View[0].Name = "Wavefield (pressure)";
View[0].Axes = 0;
View[0].AxesLabelX = "X";
View[0].AxesLabelY = "Z";


// Optional: Set visualization options, such as view angle, scale, etc.
//General.AlphaBlending = 1;
View[0].Color.Text2D = {0, 0, 0};

// Color Scale
View[0].SaturateValues = 1;
View[0].CustomMax = 0.14;
View[0].CustomMin = -0.14;
View[0].RangeType = 2;
View[0].ColormapNumber = 10;
View[0].ShowScale = 0;
Mesh.SurfaceEdges = 0;

General.ColorScheme = 0;
General.ScaleX = 1.3;
General.ScaleY = 1.3;
General.ScaleZ = 1.3;
//General.TranslationX = 0.5;
//General.TranslationY = -30;
General.TranslationX = -0.004022149699727862;
General.TranslationY = -1.508313031543774;
General.GraphicsHeight = 960;
General.GraphicsWidth = 2560;
General.GraphicsPositionX = 0;
General.GraphicsPositionY = 3;
General.MenuWidth = 640;
General.SmallAxes = 0;



// Replace 'output.png' with the desired output file name
Print.Text = 0;
Draw;
Save "champ_8hz.png";

//Print.Text = 1;
//Draw;
//Save "{tex_file}";

//Exit;
//Exit;
//+
Show "*";
//+
Hide {
  Point{1}; Point{2}; Point{3}; Point{4}; Point{5}; Point{6}; Point{7}; Point{8}; Point{9}; Point{10}; Point{11}; Point{12}; Point{13}; Point{14}; Point{15}; Point{16}; Point{17}; Point{18}; Point{19}; Point{20}; Point{21}; Point{22}; Point{23}; Point{24}; Point{25}; Point{26}; Point{27}; Point{28}; Point{29}; Point{30}; Point{31}; Point{32}; Point{33}; Point{34}; Point{35}; Point{36}; Point{37}; Point{38}; Point{39}; Point{40}; Point{41}; Point{42}; Point{43}; Point{44}; Point{45}; Point{46}; Point{47}; Point{48}; Point{49}; Point{50}; Point{51}; Point{52}; Point{53}; Point{54}; Point{55}; Point{56}; Point{57}; Point{58}; Point{59}; Point{60}; Point{61}; Point{62}; Point{63}; Point{64}; Point{65};
}
