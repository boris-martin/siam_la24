import numpy as np
import matplotlib.pyplot as plt


def extract_iterations(filename):
    # Initialize an empty list to store iteration counts
    iteration_counts = []

    # Open the file and process each line
    with open(filename, 'r') as file:
        for line in file:
            # Split the line into words and extract the iteration count
            # Assuming the format is consistent as in your example
            parts = line.split()
            iterations = int(parts[4])  # The iteration count is the 4th element (0-indexed)
            iteration_counts.append(iterations)

    # Convert the list to a NumPy array
    iteration_counts_array = np.array(iteration_counts)
    return iteration_counts_array

# Usage
filenames = ['log_gcr_reuse_4.txt', 'log_gcr_reuse_6.txt', 'log_gcr_reuse_8.txt']  # Replace with your actual file path
iterations_array = [extract_iterations(filename) for filename in filenames]
plt.plot(iterations_array[0], label="4 Hz")
plt.plot(iterations_array[1], label="6 Hz")
plt.plot(iterations_array[2], label="8 Hz")
print("Local sums for each freq : ", [np.sum(tab) + 120 for tab in iterations_array])
plt.xlabel('RHS index')
plt.ylabel('Iterations needed')
plt.title('Number of iterations for 120 sources, sequentially')
plt.legend()
plt.savefig('fig_gcr_reuse.png', dpi=300)
plt.show()

print(iterations_array)

filenames = ['basic_4.txt', 'basic_6.txt', 'basic_8.txt']  # Replace with your actual file path
iterations_array = [extract_iterations(filename) for filename in filenames]
print("Local sums for each freq : ", [np.sum(tab) + 120 for tab in iterations_array])
print("Local means for each freq : ", [np.mean(tab) + 1 for tab in iterations_array])
