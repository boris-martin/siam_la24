\begin{frame}{Sequential subspace recycling with GCR}
    The GCR algorithm is a minimal residual Krylov solver (equivalent to GMRES)
    that builds a $A^*A$ orthonormal basis of a subspace. To solve $Ax=b$:
    \begin{enumerate}
      \item Set $r_0 = b-Ax_0$
      \item For $i = 1, 2, ...$ until convergence, do:
      \begin{itemize}
          \item Pick a new direction $\tilde{u}_i = r_{i-1}$ and set $\tilde{c}_i = A\tilde{u}_i$.
          \item Make it $A^*A$ orthogonal to previous directions $u_j$ ($j < i$) with a Gram-Schmidt procedure:
          \begin{align*}
              y_j &= c_j^*\tilde{c}_i\\
              \tilde{u}_i &:= \tilde{u}_i - \sum_{j < i} u_j y_j
          \end{align*}
          \item Normalize it to get $u_i$ and $c_i = Au_i$ such that $c_i^*c_i = 1$.
          \item Compute step length $\alpha_i = c_i^*r_{i-1}$.
          \item Set $x_i = x_{i-1} + \alpha_i u-i$ and $r_i = r_{i-1} - \alpha_i c_i$.
      \end{itemize}
    \end{enumerate}
  \end{frame}
  
  \begin{frame}{Sequential subspace recycling with GCR}
    The procedure extends naturally to sequences of right hand sides\\[1ex]
    Let $U$ and $C = AU$ contain the columns of the previous directions. To solve
    for another $b$:
    \begin{enumerate}
    \item Set $x_0 = UC^* b$
    \item For $i = 1, 2, ...$ until convergence, do:
      \begin{itemize}
      \item Pick a new direction $\tilde{u}_i = r_{i-1}$ and set
        $\tilde{c}_i = A\tilde{u}_i$.
      \item Make it $A^*A$ orthogonal to directions from the previous RHS:
        $\tilde{u}_i := \tilde{u}_i - UC^*\tilde{u}_i$.
      \item Make it $A^*A$ orthogonal to previous directions from this RHS as
        previously.
      \item Normalize, compute step length and update $x_i$ as before.
      \end{itemize}
    \end{enumerate}
    \pause \bigskip
    For each additional RHS, convergence is (hopefully) \textbf{faster} but the
    space size keeps \textbf{increasing}
  \end{frame}
  
  \begin{frame}{Sequential subspace recycling with GCR}
    % Results for $S = \imath k (1 + \imath)$ (Despré's algorithm with evanescent modes damping):
    \begin{figure}
      \centering
      \includegraphics[width=0.65\textwidth,keepaspectratio]{logs/fig_gcr_reuse.png}
      % \caption{Number of iterations per RHS}
    \end{figure}
    \begin{itemize}
    \item Significant gains, even for modest number of sources
    \item As a comparison, without recycling, the average number of iterations per
      RHS is: 124 at 4Hz, 122 at 6 Hz, and 138 at 8Hz
    \end{itemize}
  \end{frame}
  
  \begin{frame}{Robustness of sequential recycling}
    \begin{itemize}
    \item Increased number of iterations when sources get close to subdomain
      interfaces
      % can be primarily attributed to the change of domain of the solutions:
      % recycled directions might not be adequate to improve the resolutions of
      % points in or close to another subdomain
      \bigskip
    \item Increased number of iterations with frequency
      \pause
      \begin{itemize}
      \item variation is actually due to distance between sources \textbf{relative
          to the wavelength $\lambda$}
      \end{itemize}
      \begin{center}
        \begin{columns}[t]
          \begin{column}{.45\textwidth}
            \centering
            \includegraphics[width=\textwidth,keepaspectratio]{figs/robust1.png}\\
            %$d_s = 0.192\lambda$
            Fix $d_s = 0.2\lambda$
          \end{column}
          \begin{column}{.45\textwidth}
            \includegraphics[width=\textwidth,keepaspectratio]{figs/robust2.png}
          \end{column}
        \end{columns}
      \end{center}
    \end{itemize}
  \end{frame}
  
  \begin{frame}{Robustness of sequential recycling}
    \begin{itemize}
    \item Choice of transmission operator $\mathcal{S}$, mesh refinement and
      finite element order have only marginal impact
      \bigskip
    \item If one caps the number of directions, the simplest recycling strategy
      (recycling the first directions) is the best; it outperforms recycling
      \begin{itemize}
      \item the last (most recent) directions
      \item directions leading to the largest residual decrease
      \item directions leading to the most significant coefficients in absolute
        value during the orthogonalization
      \end{itemize}
    \end{itemize}
  \end{frame}


  
  \begin{frame}{Block Krylov methods}
    We can also use Block GMRES (BGMRES) for faster convergence:
    \begin{itemize}
    \item Solve everything at once, and use the subspace of each RHS in all resolutions
    \item Expensive in memory...
    \item ... but the substructuring makes this bearable!
    \end{itemize}
    \pause
    \begin{table}
      \centering
      \caption{Number of local solves (matrix-vector product) for solving the same 120 sources
        on the Marmousi initial model}
      \begin{tabular}{lcccccc}
        \toprule
        Block size & 1 & 5 & 10 & 30 & 60 & 120 \\
        \midrule
        % Results for $S = \imath k (1 + \imath)$
        4 Hz  & 15 017 & 8 950 & 7 440 & 6 240 & 5 460 & 3 840 \\
        6 Hz  & 14 838 & 9 205 & 7 130 & 5 520 & 4 800 & 3 360 \\
        8 Hz  & 16 663 & 10 650 & 8 090 & 5 670 & 4 800 & 3 240 \\
  
        % Results for $S = \imath k$
        % 4 Hz  & 19 093 & 15 055 & 12 650 & 8 850 & 6 900 & 4 440 \\
        % 6 Hz  & 15 830 & 14 240 & 12 270 & 8 610 & 6 960 & 4 320 \\
        % 8 Hz  & 16 488 & 15 370 & 14 250 & 9 450 & 7 380 & 4 320 \\
        \bottomrule
      \end{tabular}
    \end{table}
    % (Problem sizes are 6 848, 10 192, 13 536.)\\
    \pause
    \bigskip
    \begin{itemize}
    \item Larger blocks always lead to a faster convergence
    \item Robust w.r.t. the choice of interface operator $\mathcal{S}$
    \end{itemize}
  \end{frame}